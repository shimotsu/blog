---
title: 感性を蘇生
date: "2020-08-23T00:00:00.000Z"
---

トロログの<a href="https://toro-log.hateblo.jp/entry/2020/08/18/235648" target="_blank">この記事</a>を見て、YouTubeのコメントや再生回数を消したり、Twitterのいいね／RT数を消したりしているが、すこぶる調子がいい。「あぁ、俺がやるべきはコレだったんだな」と直感的に分かった。

詳しくは上記の記事を読んで欲しいが、そもそもYouTubeのコメントを無意識に追っかけてしまっていることに対する危機感は、1年くらい前からずっとうっすら感じていた。動画と一緒にコメントを見る、というかコメントありきで動画を見るみたいな感じでコンテンツを消化するようになっていて、あるときふとそれを完全に無意識にやっている自分に気づき、本当に怖くなってしまった。

なぜ怖いと思ったのか。

単刀直入に言うと、自分の感性がどんどん死んでいってる感じがした。「死ぬ」というと大げさかもしれないが、「感性が鈍る」とかそういう生ぬるい表現じゃ済まされないほど、そのコンテンツを見て「自分がどう感じたか」をはかるセンサが衰えているのを感じた。あたかも、コメントされている内容（そのなかでも特に多くの共感を得ているもの）がまるで自分の意見かのように感じ、自分も例外でないと安心し、コンテンツを咀嚼していた。そういう意味で、自分の感性はかなり死にかけてると思った。だって、YouTubeの動画を再生して、まず先にコメント欄見に行くなんて普通じゃない。終わってる。

「いやいや、たかがYouTubeのコメントごときで。笑」と笑われてしまうだろうけど、この日々の積み重ねは後々取り返しのつかないことになるんじゃないかと、そういう予感があったので、ここらでどうにかして手を打たなければと思っていた矢先にこの「YouTubeのコメントを消す」というのをやってみた、という経緯である。

実際に試してみると、最初のうちはコメントがないことが不便で「どんなことが書いてあるんだろう」と気になる。けど、それを我慢してずっと見てると、ちょっとずつだがこの状況が気にならなくなってくる。そして、もう一つ現れた変化として、ちゃんと動画を見るようになった。なぜかというと、コメントを見て動画の内容をジャッジできないので、ずっとちゃんと刮目しているしかないからだ。もう、ほぼリハビリである。で、このリハビリも続けていると、自然とコメント欄のことがどうでもよくなってくる。これは目覚ましい成果で、まさにこの状態を望んでいた。なので、いますこぶる調子がいい。

YouTubeのコメントに限った話ではなく、本でも映画でも料理でも店でも、なんでも一切のレビューを無視して自分の感性だけで生きていけるように仕上げていきたい。