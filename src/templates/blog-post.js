import React from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../components/layout"
import SEO from "../components/seo"

const BlogPostTemplate = ({ data, pageContext, location }) => {
  const post = data.markdownRemark
  const siteTitle = data.site.siteMetadata.title
  const { previous, next } = pageContext

  const shareUrl = `https://twitter.com/share?url=${data.site.siteMetadata.siteUrl}${data.markdownRemark.fields.slug}&text=${post.frontmatter.title}`

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title={post.frontmatter.title} description={post.excerpt} />
      <Article>
        <DATE>{post.frontmatter.date}</DATE>
        <header>
          <h1>{post.frontmatter.title}</h1>
        </header>
        <section dangerouslySetInnerHTML={{ __html: post.html }} />
      </Article>
      <ButtonWrapper>
        <Button>
          <a
            href={shareUrl}
            target="_blank"
            rel="noopener noreferrer"
          >
            Tweet
          </a>
        </Button>
      </ButtonWrapper>
      <Nav>
        <ul
          style={{
            display: `flex`,
            flexWrap: `wrap`,
            justifyContent: `space-between`,
            listStyle: `none`,
            padding: 0,
          }}
        >
          <PREV>
            {previous && (
              <Link to={previous.fields.slug} rel="prev">
                ← {previous.frontmatter.title}
              </Link>
            )}
          </PREV>
          <NEXT>
            {next && (
              <Link to={next.fields.slug} rel="next">
                {next.frontmatter.title} →
              </Link>
            )}
          </NEXT>
        </ul>
      </Nav>
    </Layout>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        siteUrl
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date(formatString: "YYYY.MM.DD")
      }
      fields {
        slug
      }
    }
  }
`

const Article = styled.article`
  margin-bottom: 30px;
  header {
    margin: 0 0 20px;
    @media (max-width: 768px) {
      margin: 0 0 20px;
    }
  }
  h1 {
    font-size: 26px;
    font-weight: 700;
    line-height: 1.6;
    @media (max-width: 768px) {
      font-size: 20px;
      line-height: 1.6;
    }
  }
  h2 {
    margin-bottom: 10px;
    font-size: 22px;
    font-weight: bold;
    @media screen and (max-width: 780px) {
      font-size: 18px;
      line-height: 1.8;
    }
  }
  ul {
    padding-left: 20px;
    margin-bottom: 20px;
  }
  a {
    padding: 1px 2px;
    color: #5183f5;
    font-weight: bold;
    text-decoration: none;
    transition: all 0.3s;
    &:hover {
      opacity: 0.7;
    }
  }
`

const Nav = styled.nav`
  a {
    text-decoration: none;
    transition: all 0.3s;
    &:hover {
      opacity: 0.7;
    }
  }
`
const ButtonWrapper = styled.div`
  margin-bottom: 40px;
  text-align: right;
`

const Button = styled.div`
  a {
    display: inline-block;
    padding: 0px 8px;
    border: 2px solid #000000;
    border-radius: 4px;
    cursor: pointer;
    transition: all 0.3s;
    font-size: 12px;
    font-weight: bold;
    text-decoration: none;
    &:hover {
      border: 2px solid #000000;
      background-color: #000000;
      color: #ffffff;
    }
  }
`

const PREV = styled.li`
  width: 100%;
  display: block;
  text-align: left;
`

const NEXT = styled.li`
  width: 100%;
  display: block;
  text-align: right;
`

const DATE = styled.span`
  font-size: 11px;
  font-weight: bold;
  color: #000000;
`
