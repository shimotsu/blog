import React from "react"
import GlobalStyle from "../../style/GlobalStyle"
import Wrapper from "./style.js"

import Header from '../Header'

const Layout = ({ location, title, children }) => {

  return (
    <Wrapper>
      <Header />
      <main>{children}</main>
      <GlobalStyle />
    </Wrapper>
  )
}

export default Layout
