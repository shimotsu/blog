import styled from "styled-components"

const Wrapper = styled.div`
  max-width: 640px;
  margin: 40px auto 80px;
  @media (max-width: 768px) {
    margin: 30px auto 80px;
  }
`

export default Wrapper
