import React from "react"
import { Link } from "gatsby"
import Wrapper from "./style.js"

const Header = () => {
  return (
    <Wrapper>
      <h1>
        <Link to="/">shimotsu Blog</Link>
      </h1>
      <p>
        <Link to="/profile">Profile</Link>
      </p>
    </Wrapper>
  )
}

export default Header
