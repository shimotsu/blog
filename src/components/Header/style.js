import styled from "styled-components"

const Wrapper = styled.header`
  margin-bottom: 30px;
  text-align: center;
  h1 a {
    font-family: "Noto Sans JP", "Roboto", "Helvetica Neue", Helvetica, Arial, "游ゴシック体",
      "Yu Gothic", YuGothic, "ヒラギノ角ゴ Pro", "Hiragino Kaku Gothic Pro",
      "メイリオ", Meiryo, "MS Pゴシック", "MS PGothic", sans-serif;
    font-size: 42px;
    font-style: italic;
    text-decoration: none;
    @media (max-width: 768px) {
      font-size: 32px;
    }
  }

  p a {
    font-size: 14px;
    color: #000;
  }
`

export default Wrapper
