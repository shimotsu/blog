import { css, createGlobalStyle, keyframes } from "styled-components"
import { rgba } from "polished"

/* ===============================================
#  color setting
=============================================== */
export const colors = {}

colors.background = "#ffffff"
colors.text = "#111111"

colors.paleGray = "#f9f9f9"
colors.lightGray = "#ddd"
colors.gray = "#96acb3"

colors.primaryPurple = "#9E69D7"
colors.primary = "#FA96BB"
colors.primaryLight = rgba("#FA96BB", 0.15)
colors.primaryPale = rgba("#FA96BB", 0.07)

colors.secondary = "#FFB238"
colors.danger = "#FF5C55"
colors.success = "#4FCE7A"
colors.warn = "#feaa2e"
colors.info = "#2490eb"

/* ===============================================
#  other variables
=============================================== */

export const fadeIn = keyframes`
  0% {
    opacity: 0;
    visibility: hidden;
    transform: translateY(10px);
  }
  100% {
    opacity: 1;
    visibility: visible;
    transform: translateY(0);
  }
`

export const fadeOut = keyframes`
  0% {
    opacity: 1;
    visibility: visible;
    transform: translateY(0);
  }
  100% {
    opacity: 0;
    visibility: hidden;
    transform: translateY(-10px);
  }
`

export const size = {
  width: "640px",
  topHeight: "370px",
}

/* ===============================================
#  set css variables
=============================================== */
function setColor() {
  let styles = ""
  for (const key in colors) {
    styles += `--${key}: ${colors[key]};`
  }
  return css`
    :root {
      ${styles}
      --width: ${size.width};
      --topHeight: ${size.topHeight};
    }
  `
}

/* ===============================================
#  font setting
=============================================== */
const font = css`
  font-family: "Noto Sans JP", "Helvetica Neue", Helvetica, Arial, 游ゴシック体, "Yu Gothic", YuGothic, "ヒラギノ角ゴ Pro", "Hiragino Kaku Gothic Pro", メイリオ, Meiryo, "MS Pゴシック", "MS PGothic", sans-serif;
  font-style: normal;
  font-weight: 400;
  word-wrap: break-word;
  word-break: break-all;
  -moz-font-feature-settings: "palt";
  -webkit-font-feature-settings: "palt";
  font-feature-settings: "palt";
  font-variant-ligatures: no-common-ligatures;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-rendering: auto;
  -webkit-text-stroke: 1px transparent;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  letter-spacing: 0.05rem;
  color: var(--text);
`

/* ===============================================
#  global style
=============================================== */
const GlobalStyle = createGlobalStyle`
  ${setColor}
  body {
    ${font}
    font-size: 16px;
    line-height: 2;
    padding: 0;
    margin: 0;
    position: relative;
    background: var(--background);
    a {
      color: var(--text);
    }
  }
  * {
    box-sizing: boreder-box;
    margin: 0;
    padding: 0;

  }
  article {
    margin-bottom: 20px;
  }
  h1 {
    font-weight: 900;
  }
  h2, h3, h4, h5, h6 {
    font-weight: 400;
  }
  h3 {
    font-size: 14px;
    font-weight: bold;
    text-decoration: none;
  }
  p {
    font-size: 18px;
    margin-bottom: 36px;
    line-height: 2;
    @media screen and (max-width: 780px) {
      font-size: 16px;
      margin-bottom: 30px;
    }

    > a {
      color: #00139e;
      &:hover {
        opacity: 0.6;
      }
    }
  }
  *:before, *:after {
    box-sizing: border-box;
  }
  input, button, textarea, button, select {
    ${font}
    border: none;
    outline: none;
    margin: 0;
    padding: 0;
    border: none;
    outline: none;
    background: none;
    line-height: 1.5;
    -webkit-appearance: none;
    &:focus {
      border: none;
      outline: none;
    }
  }
  ::selection {
    color: #111111;
    background: rgba(12,8,141,0.20);
  }

  /* ===============================================
  # smart phone
  =============================================== */
  @media screen and (max-width: 780px) {
    html, body {
      padding: 0 15px;
    }
    .pc {
      display: none !important;
    }
  }

  /* ===============================================
  # pc
  =============================================== */
  @media screen and (min-width: 781px) {
    .sp {
      display: none !important;
    }
  }
`

export default GlobalStyle
