import React from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../components/layout"
import SEO from "../components/seo"

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title
  const posts = data.allMarkdownRemark.edges

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title="Top" />
      {posts.map(({ node }) => {
        const title = node.frontmatter.title || node.fields.slug
        return (
          <Article key={node.fields.slug}>
            <Link to={node.fields.slug}>
              <DATE>{node.frontmatter.date}</DATE>
              <header>
                <h3>{title}</h3>
              </header>
            </Link>
          </Article>
        )
      })}
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt(pruneLength: 100)
          fields {
            slug
          }
          frontmatter {
            date(formatString: "YYYY.MM.DD")
            title
          }
          htmlAst
        }
      }
    }
  }
`
const Article = styled.article`
  font-size: 16px;
  @media (max-width: 768px) {
    margin-bottom: 15px;
  }
  & h3 {
    font-size: 20px;
    line-height: 1.6;
    @media screen and (max-width: 768px) {
      font-size: 16px;
    }
  }
  & p {
    font-size: 12px;
    line-height: 1.8;
  }
  &:hover {
    opacity: 0.5;
  }
  > a {
    text-decoration: none;
  }
`

const DATE = styled.span`
  font-size: 11px;
  font-weight: bold;
  color: #000000;

  @media screen and (max-width: 768px) {
    font-size: 10px;
  }
`
