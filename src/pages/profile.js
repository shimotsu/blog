import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Icon from "../images/icon_shimotsu.jpg"

const Profile = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title="Profile" />
      <div>
        <Name>shimotsu</Name>
        <Img src={Icon} alt="shimotsu"></Img>
        <Text>1993年、鹿児島県生まれ。新卒でデジタルマーケティングを手がける会社に入社し、Web編集者としてキャリアをスタート。2016年にはディレクター兼COOとしてWeb制作会社・株式会社LuckyBrothers.co.jpを創業、取締役として約3年間働く。2019年7月より株式会社トゥーアール（to-R）にてフロントエンドエンジニアとして働いている。</Text>
        <Text><a href="https://scrapbox.io/shimotsu/" target="_blank" rel="noopener noreferrer">Scrapbox</a> / <a href="https://twitter.com/shimotsu_" target="_blank" rel="noopener noreferrer">Twitter</a> / <a href="https://shimotsu.hatenablog.com/" target="_blank" rel="noopener noreferrer">Tech Blog</a> / <a href="https://shimoqui.com/" target="_blank" rel="noopener noreferrer">Podcast</a></Text>
      </div>
    </Layout>
  )
}

export default Profile

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`

const Name = styled.h1`
  font-size: 20px;
  font-weight: bold;
  color: #000000;
`

const Text = styled.p`
  margin-bottom: 10px;
  font-size: 15px;
  color: #000000;
`

const Img = styled.img`
  width: 200px;
  height: 200px;
`
